package edu.pnw.softwaredesign.tollbooth;

/**
 * Creates a TollBooth for the incoming trucks.
 * 
 * @author Piyush
 *
 */
public interface TollBooth {
  /**
   * Calculates toll.
   * 
   * @param truck type of Truck.
   */
  public void calculateToll(Truck truck);
  
  /**
   * Displays TollBooth's info.
   */
  public void displayData();
}
