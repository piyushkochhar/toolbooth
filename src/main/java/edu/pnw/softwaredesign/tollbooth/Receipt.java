package edu.pnw.softwaredesign.tollbooth;

/**
 * Prints the Receipt.
 * 
 * @author Piyush
 *
 */
public class Receipt {
  /**
   * Prints the Receipt.
   * 
   * @param boothTotal Total booth collection
   * @param noOfTrucks Total number of trucks passed at a booth
   */
  public static void print(int boothTotal, int noOfTrucks) {
    System.out.println("\n*** Collecting receipts  ***");
    System.out.println("Totals since the last collection - Receipts: $" + boothTotal + " Trucks: "
        + noOfTrucks + "\n");
  }
}
