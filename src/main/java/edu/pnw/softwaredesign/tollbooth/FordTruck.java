package edu.pnw.softwaredesign.tollbooth;

/**
 * Creates an object of type FordTruck that implements Truck.
 * 
 * @author Piyush
 *
 */
public class FordTruck implements Truck {
  
  int axles;
  
  int weight;
  
  public int getAxles() {
    return axles;
  }
  
  public void setAxles(int axles) {
    this.axles = axles;
  }
  
  public int getWeight() {
    return weight;
  }
  
  public void setWeight(int weight) {
    this.weight = weight;
  }
  
  public FordTruck(int axles, int weight) {
    this.axles = axles;
    this.weight = weight;
  }
}
