package edu.pnw.softwaredesign.tollbooth;

/**
 * Creates an object of type DaewooTruck that implements Truck.
 * 
 * @author Piyush
 *
 */
public class DaewooTruck implements Truck {
  
  int axles;
  
  int weight;
  
  public int getAxles() {
    return axles;
  }
  
  public void setAxles(int axles) {
    this.axles = axles;
  }
  
  public int getWeight() {
    return weight;
  }
  
  public void setWeight(int weight) {
    this.weight = weight;
  }
  
  public DaewooTruck(int axles, int weight) {
    this.axles = axles;
    this.weight = weight;
  }
}

