package edu.pnw.softwaredesign.tollbooth;

/**
 * Creates an object of type NissanTruck that implements Truck.
 * 
 * @author Piyush
 *
 */
public class NissanTruck implements Truck {
  
  int axles;
  
  int weight;
  
  public int getAxles() {
    return axles;
  }
  
  public void setAxles(int axles) {
    this.axles = axles;
  }
  
  public int getWeight() {
    return weight;
  }
  
  public void setWeight(int weight) {
    this.weight = weight;
  }
  
  public NissanTruck(int axles, int weight) {
    this.axles = axles;
    this.weight = weight;
  }
}

