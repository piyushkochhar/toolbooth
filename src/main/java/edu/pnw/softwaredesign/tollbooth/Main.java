package edu.pnw.softwaredesign.tollbooth;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Testing the TollBooth Class.
 * 
 * @author Piyush
 *
 */

public class Main {
  
  public static void main(String[] args) {

    ApplicationContext context = new FileSystemXmlApplicationContext("spring.xml");
    
    TollBooth booth = (AlleghenyTollBooth) context.getBean("tollBoothAllegheny");
    
    Truck ford = (Truck) context.getBean("fordTruck");
    booth.calculateToll(ford);
    booth.displayData();
    
    Truck nissan = (Truck) context.getBean("nissanTruck");
    booth.calculateToll(nissan);
    booth.displayData();
    
    Truck daewoo = (Truck) context.getBean("daewooTruck");
    booth.calculateToll(daewoo);
    booth.displayData();
    
  }

}
