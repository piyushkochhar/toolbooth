package edu.pnw.softwaredesign.tollbooth;

/**
 * Interface for objects of type Truck.
 * 
 * @author Piyush
 *
 */
public interface Truck {
  /**
   * Gets the axles.
   * 
   * @return axles
   */
  public int getAxles();
  
  /**
   * Sets the axles.
   * 
   * @param axles axles of truck
   */
  public void setAxles(int axles);
  
  /**
   * Gets the weight.
   * 
   * @return weight
   */
  public int getWeight();
  
  /**
   * Gets the weight.
   * 
   * @param weight weight of truck
   */
  public void setWeight(int weight);
}
