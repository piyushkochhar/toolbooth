package edu.pnw.softwaredesign.tollbooth;

/**
 * Implements the TollBooth Interface.
 * 
 * @author Piyush
 *
 */
public class AlleghenyTollBooth implements TollBooth {
  int boothTotal;
  int noOfTrucks;
  int toll;
  Truck truck;
  
  public int getBoothTotal() {
    return boothTotal;
  }
  
  public int getNoOfTrucks() {
    return noOfTrucks;
  }
  
  public int getToll() {
    return toll;
  }
  
  public Truck getTruck() {
    return truck;
  }
  
  /**
   * Calculates toll.
   */
  public void calculateToll(Truck truck) {
    this.truck = truck;
    toll = 5 * truck.getAxles() + truck.getWeight() / 100;
    boothTotal += toll;
    noOfTrucks++;
    System.out.println(PrintTruckInfo.print(truck) + " Toll due: $" + toll);
  }
  
  /**
   * Displays Receipts.
   */
  public void displayData() {
    Receipt.print(boothTotal, noOfTrucks);
  }
  
}
