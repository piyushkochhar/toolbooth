package edu.pnw.softwaredesign.tollbooth;

/**
 * Prints the Truck Info.
 * 
 * @author Piyush
 *
 */
public class PrintTruckInfo {
  /**
   * Prints the Truck info.
   * 
   * @param truck type of Truck
   * @return String with Truck's info
   */
  public static String print(Truck truck) {
    return "Truck arrival - Axles: " + truck.getAxles() + " Total weight: " + truck.getWeight();
  }
}
