package edu.pnw.softwaredesign.tollbooth;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import junit.framework.TestCase;

/**
 * Unit test.
 */
public class AppTest extends TestCase {
  
  ApplicationContext context = new FileSystemXmlApplicationContext("spring.xml");
  
  AlleghenyTollBooth booth = (AlleghenyTollBooth) context.getBean("tollBoothAllegheny");
  
  /**
   * Tests values of Ford Truck.
   */
  public void testFordTruck() {
    
    Truck ford = (Truck) context.getBean("fordTruck");
    booth.calculateToll(ford);
    
    assertEquals(1, booth.getNoOfTrucks());
    assertEquals(145, booth.getToll());
    assertEquals(145, booth.getBoothTotal());
  }
  
  /**
   * Tests values of Nissan Truck.
   */
  public void testNissanTruck() {
    Truck nissan = (Truck) context.getBean("nissanTruck");
    booth.calculateToll(nissan);
    
    assertEquals(1, booth.getNoOfTrucks());
    assertEquals(60, booth.getToll());
    assertEquals(60, booth.getBoothTotal());
  }
  
  /**
   * Tests values of Daewoo Truck.
   */
  public void testDaewooTruck() {
    Truck daewoo = (Truck) context.getBean("daewooTruck");
    booth.calculateToll(daewoo);
    
    assertEquals(1, booth.getNoOfTrucks());
    assertEquals(200, booth.getToll());
    assertEquals(200, booth.getBoothTotal());
  }
  
  /**
   * Tests values of multiple trucks.
   */
  public void testManyTrucks() {
    Truck ford = (Truck) context.getBean("fordTruck");
    Truck nissan = (Truck) context.getBean("nissanTruck");
    Truck daewoo = (Truck) context.getBean("daewooTruck");
    booth.calculateToll(daewoo);
    booth.calculateToll(nissan);
    booth.calculateToll(ford);
    
    assertEquals(3, booth.getNoOfTrucks());
    assertEquals(405, booth.getBoothTotal());
    
  }
}
